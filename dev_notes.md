# Development Notes

## Intention

A weather polling and query tool set for setting up local polling on one's computer.

Allows an individual to construct their own queries to watch for weather conditions propitious to a given activity.

UI:

* Project should provide a simple desktop alerting call to allow simple scripters to write their alert and set it on a cron.
* Project may provide a web interface for hosting on a server

### Target data

Main weather

* cloud cover
* temperature
* sunrise/sunset points for day/night specification
    * morning/evening twilight durations ?
* wind speed
* atmospheric pressure
* humidity
    * chance of precipitation

General phenomena

* Moon phases

and extended phenomena (provided by WeatherWatch plug-ins):

* tides (specific coastal stations)
    * level
    * direction
    * wave intensity (is this a thing?) (e.g. for coastal safety, or surfers)
* KP value for aurora (restricted to particular latitudes ?)
* waterway flooding (locations with flooding data points)
* constellation presence
* meteor shower presence
* Eclipse (with progress and visibility weightings)
* other arbitrary phenomena

### Sources

Some examples:

* Open Weather Map: <https://openweathermap.org/api>
* UK tide level services: <https://www.api.gov.uk/ea/tide-gauge/#tide-gauge>
  * (does this provide forecasts around the world ?)
* AuroraWatch UK: <https://aurorawatch.lancs.ac.uk/api-info/>


## Modules required

* POLL - to query sources, and write to VAULT
* QUERY - abstract API (and reference implementation) to query a forecast VAULT
* VAULT - to store and retrieve forecast results
    * Storage should be agnostic of source, but should support source's original data

Desirable

* WEB - simple query UI over a web interface

Each module may make use of a shared set of supporting libraries (e.g. the database API), but a module itself should be usable independently.

## Forecast precision

Different amounts of time lookahead require different levels of precision. Data returned from sources may offer different levels upfront, or it may be required to specify what ranges. The goal is to store data as follows:

    * for 0 and 1 day ahead, store a by-the-hour forecast
    * for 2 day ahead, store forecasts for 3 hour increments, from midnight
    * for 3 days ahead and more, store forecasts for 6 hours increments, from midnight
        * if precision is not available, distribute available data accross all slots

## Forecast Storage Module (VAULT)

VAULT needs to provide a source-agnostic storage and retrieval interface

* Store a forecast for a location with
    * the source's original forecast data (like, "overcast", "intermittent showers", "sunny", "5", etc)
    * a normalised approximation ("overcast", "rainy", "clear")
        * normalised values (use internal value models for qualitative data)
    * timestamp of retrieval
    * timestamp of forecast (with minutes and seconds zeroed)
      * one point per hour , registered for a time on the hour
      * a "forecast point"
    * source ID
    * country, town as separate entries, optional value for state
* Lookup a forecast for a location
    * start and end timestamps
    * location
    * source
    * optionally, specify to return only latest forecast for a given forecast timestamp
* A cleanup routine
    * remove any forecast data older than specified timestamp
    * to be called externally as-needed

Supported databases:

* SQLite3 - native to Python
* MySQL - popular

Support for other databases can be added separately.

## Website Query Interface (WEB)

WEB should provide a simple HTML Form interface, allowing a basic query:

* Specify a list of locations
* Specify a list of weather conditions to look for
* Specify which day range to look ahead to (a pair of numbers)

The query should be passed via a HTTP GET so that the user can bookmark the URL.

Ideally, the interface's query model should be able to map direectly to the QUERY fluent interface.

## Forecast Source Polling (POLL)

POLL needs to provide an extensible framework in which to add support for arbitrary sources. At its heart, the following should be possible:

* Specify a location specified by long/lat
* Specify the number of days ahead to look (1-7 probably)
* Specify multiple locations in a single high-level query (underlying query may make multiple requests if necessary)
    * This is to maximize use of a poll-query to reduce API rate usage
    * Plug-in for specific source determines how to execute queries against the source, but the abstraction should allow multiple locations

## Forecast Database Querying (QUERY)

An API to query the database is specified as a Fluent Interface like the following examples.

Ideally, the Fluent Interface API is specified abstractly, and a reference implementation would be provided.

The Fluent Interface design should allow translating between it and higher-level abstractions (e.g. UI query blocks, custom query language, etc)

The following is an example of a Python-based implementation of the API.

---

A forecast check for how many days with significant enough rain - maybe to plan garden watering ? A simple query.

```python

watering_forecast = (
  Forecast("London, ON, CA") # A new forecast query object "City, Country" or "City, State, Country"
  .look_forward(days=7) # define days from now to N days (just tomorrow = 1)
  .get_weather(rain_check=lambda chance: chance > 70) # get_weather has *_check items for standard features
).days(with_points=4) # only include days with more than 4 matching forecast points per day

if watering_forecast:
    DekstopAlert(f"There will be {len(watering_forecast)} days with sufficient rain for my garden.")
```

---

A forecast check for weather conditions for planning a week-end getaway for stargazing. Restricts days and times using `check_times()`, and chains multiple `get_weather()` uses.

```python
star_towns = ["Moffat, UK", "Aviemore, GB"]
for town in star_towns:
  stargaze_forecast = (Forecast(town)
      .look_forward(days=5)
      .check_times( # restrict query set to entries matching specific days
          days=["friday","saturday"],
          times="18:00-01:00" # allow time spec over midnight ?
      )
      .get_weather(temp_check=lambda T: 5 <= t <= 15)
      .get_weather(cloud_check=lambda cover: 0 <= cover < 5) # use our own cloud cover quantification
      ).days(with_points=5)
  if stargaze_forecast:
      # If anything found, we can simply say when
      DekstopAlert(f"Found weekend star-gazing nights at {town}: {stargaze_forecast.show(format='ddd')}")
      # Should print out days using formatting, like "Friday,Saturday"
```

---

Any days with walkable times during daytime at Mont St Michel (the island in France whose walkway floods at high tide). Makes use of `check_times(light_times=True)` to check for daytime, and `with_phenomena()` for a custom phenomenon.

```python
def tide_check(tide):
    (assert tide.level < 20 or
    assert tide.direction == "falling")

tide_forecast = (Forecast("Mont Saint Michel, FR")
    .look_forward(days=5)
    .check_times(light_times=True)
    .get_weather(precipitation_check=lambda chance: 0 <= chance < 40)
    # A generic with_phenomena looks for a phenomena by name, and uses a generic check with all its data
    .with_phenomena(name="tide", check=tide_check)
    ).days(with_points=2)

if tide_forecast:
    DekstopAlert(f"Found low tide at {tide_forecast.location()} during day time for: {tide_forecast.show(display='human', include_time=True)}")
    # should print out human dates like "Tue 28th Feb 2023 (9:00 - 14:00), Wed 1st Mar 2023 (10:00 - 15:00)"
```

