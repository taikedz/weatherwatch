# WeatherWatch

A weather watching script that uses online data to check for forecasts.

Can be set up to trigger alerts when certain conditions are met. (end-goal)

## CLI usage

Setup:

```
python3 -m venv weather.venv
. weather.venv/bin/activate
pip3 install -r requirements.txt
```

Run a forecast:

```sh
python3 ./weatherwatch/pull-data.py your_config.yaml town_id
```

More to come
