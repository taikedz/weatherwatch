import sys
import yaml
import json
import urllib.request

import unpacker
import forecast
import days

def main():
    config = loadConfig(sys.argv[1])
    location = unpacker.readPath("WWConfig/locations/{}/coords".format(sys.argv[2]), config)
    api_url = buildApiUrl(config, location)
    api_name = unpacker.readPath("WWConfig/user/api", config)
    datasource = unpacker.readPath(f"WWConfig/api_list/{api_name}/datasource", config)

    json_data = callApi(api_url)
    forecast_pairs = forecast.getForecastPairs(json_data, datasource)

    printForecastPairs(forecast_pairs)

def callApi(url):
    with urllib.request.urlopen(url) as web_reader:
        return json.load(web_reader)


def loadConfig(filename):
    with open(filename) as fh:
        # full_load to avoid CVE-2017-18342
        return yaml.full_load(fh)


def buildApiUrl(config, location):
    user_config = unpacker.readPath("WWConfig/user", config)
    api_url = unpacker.readPath("WWConfig/api_list/{}/url".format(user_config['api']), config)
    api_config = unpacker.readPath("WWConfig/api_configs/{}".format(user_config['config']), config)
    api_config = {**api_config, **location}

    return api_url.format(**api_config)


def printForecastPairs(pairs):
    for t,w in pairs.items():
        t = "%s %s" % (days.getTime(int(t) ) , days.getDay(int(t)) )
        print(f"{t} -- {w}")

main()
