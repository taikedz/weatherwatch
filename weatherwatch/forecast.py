import WeatherwatchErrors as WE
import unpacker

def getForecastPairs(jsondata, datasource):
    if datasource == "openweathermap_onecall":
        return getOwmForecastPairs(jsondata)
    else:
        raise WE.UnsupportedDatasource(f"Unknown datasource '{datasource}'")

# Specific extractors:

def getOwmForecastPairs(jsondata):
    times = unpacker.readPath("daily/*/dt", jsondata)
    weathers = unpacker.readPath("daily/*/weather/0/main", jsondata)

    return dict(zip(times, weathers))

