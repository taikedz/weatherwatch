from datetime import datetime

def getTime(unix_ts):
    return datetime.fromtimestamp(unix_ts).strftime("%Y-%m-%d %I:%M:%S")

def getDay(unix_ts):
    return datetime.fromtimestamp(unix_ts).strftime("%A")

def main():
    import sys

    for unix_ts in sys.argv[1:]:
        unix_ts = int(unix_ts)
        t = getTime(unix_ts)
        d = getDay(unix_ts)
        print(f"{unix_ts} --> {d} ({t})")

if __name__ == "__main__":
    main()
