import copy
import re

STRTYPES= [str, int, float, bool]

def readPath(path, structdata):
    """ Given a path down a structure, each dereference separated by "/"
    return the data structure at the end of that path.

    Where an array is expected, either use a numerical index, or '*' to iterate all items.
    """
    structdata = copy.deepcopy(structdata)
    path = path.lstrip('/').rstrip('/').split('/')

    for idx,token in enumerate(path):
        if token == '':
            continue

        elif token == '*':
            forked_path = '/'.join(path[idx+1:])
            structdata = [readPath(forked_path, dataitem) for dataitem in structdata ]

            # The path was followed to end in the above readPath, so we now return
            return structdata

        else:
            if re.match('^[0-9]+$', token):
                token = int(token)
            structdata = structdata[token]


    return structdata

def unpackStr(pathlist, structdata):
    """ Given a list of paths, return an array of results for each path.

    Results are converted to string representation - ints, floats and bools are all converted directly to
     string representations. Other types are represented by a string decribing their type name.

    Intended as an exploratory tool for manipulating/testing nested data structures on the command line.
    """
    items = []

    if len(pathlist) < 1:
        items.append(type_convert(structdata) )
    else:
        for path in pathlist:
            data = readPath(path, structdata)
            items.append(type_convert(data))


    return items


def stringify(data):
    if type(data) in STRTYPES:
        return str(data)
    return str(type(data))


def type_convert(data):
    t = type(data)
    if t is dict: # Print as if the keys were folders
        return str( [datum + "/" for datum in data.keys()] )

    elif t is list: # Print values with a likely unused character as joiner
        for item in data:
            if not type(item) in STRTYPES:
                # Return a list of strings of types. Useful for exploratory querying
                return str([str(type(thing)) for thing in data])
        return [stringify(thing) for thing in data] #';'.join(data)

    else:
        return str(data)
